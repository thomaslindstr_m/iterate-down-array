// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var iterateDownArray = require('./index.js');
    var assert = require('assert');

    describe('iterate-down-array', function () {
        var array = [1, 2, 3, 4, 5];

        it('should iterate down the arrays 5 indices', function () {
            var i = 0;

            iterateDownArray(array, function () {
                i++;
            });

            assert.equal(5, i);
        });

        it('should let the first iteration start at the back', function () {
            var first = null;

            iterateDownArray(array, function (index, i, end) {
                first = index;
                end();
            });

            assert.equal(5, first);
        });

        it('should be able to stop iteration', function () {
            var iterations = 0;

            iterateDownArray(array, function (index, i, end) {
                iterations++;
                end();
            });

            assert.equal(1, iterations);
        });

        it('should be able to return the data passed to the end function', function () {
            var data = iterateDownArray(array, function (index, i, end) {
                end('data');
            });

            assert.equal('data', data);
        });

        it('should be able to return an array of data if more arguments were passed', function () {
            var data = iterateDownArray(array, function (index, i, end) {
                end('data', 'is', 'very', 'cool');
            });

            assert.equal(4, Object.keys(data).length);
        });
    });
